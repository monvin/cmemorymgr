CC=gcc
LD=gcc
CFLAGS= -std=c99 -Wall
LDFLAGS= 
DEPS = 
EXEMPLE1OBJ = exemple1.o
EXEMPLE2OBJ = exemple2.o

EXEC = exemple1 exemple2 


%.o: %.c $(DEPS)
		$(CC) -c $(CFLAGS) $<

all: $(EXEC)

exemple1: $(EXEMPLE1OBJ)
		$(LD) $(LDFLAGS) -o $@ $^
		echo "\n"
		./$@

exemple2: $(EXEMPLE2OBJ)
		$(LD) $(LDFLAGS) -o $@ $^
		echo "\n"
		./$@

clean:
	rm -f *.o $(EXEC)

