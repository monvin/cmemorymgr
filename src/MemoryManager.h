//
// Created by nivicos on 6/29/16.
//

#ifndef CMEMORYMANAGER_MEMORYMANAGER_H
#define CMEMORYMANAGER_MEMORYMANAGER_H

#include <stdlib.h>


#define ALLOCATION_VECTOR_SIZE 10000
typedef long long ptrsize;

typedef struct _MemoryMgr
{
    void *ptr;
    size_t size;
    ptrsize functionID;
    int line;
}MemoryMgr, *MemoryMgrPtr;

void* abstract_malloc(size_t size, ptrsize functionID, int line);
int getPointerReference(void *ptr);
void abstract_free(void *ptr);
void free_all(ptrsize functionID);
void Log_Memory();

#define Abstract_malloc(size) abstract_malloc(size, (ptrsize)&(__FUNCTION__), __LINE__)
#define Abstract_free(ptr) abstract_free(ptr)
#define Free_all() free_all((ptrsize)&__FUNCTION__)

#endif //CMEMORYMANAGER_MEMORYMANAGER_H
