//
// Created by nivicos on 6/29/16.
//

#include <stdlib.h>
#include <stdio.h>

#include "MemoryManager.h"

static MemoryMgr global_allocation[ALLOCATION_VECTOR_SIZE] = {0};
static size_t global_allocatedPointers = 0;

void* abstract_malloc(size_t size, ptrsize functionID, int line)
{
    void *memBlock = malloc(size);

    if(memBlock)
    {
        global_allocation[global_allocatedPointers].ptr = memBlock;
        global_allocation[global_allocatedPointers].size = size;
        global_allocation[global_allocatedPointers].functionID = functionID;
        global_allocation[global_allocatedPointers++].line = line;
    }
    else
        fprintf(stderr, "Error trying to allocate");

    return memBlock;

}

int getPointerReference(void *ptr)
{
    int index = 0;

    if(!ptr)
        return -1;

    for(index = 0; index <= global_allocatedPointers; index++)
    {
        if(global_allocation[index].ptr == ptr)
            return index;
    }
    return -1;
}

void abstract_free(void *ptr)
{
    int index = getPointerReference(ptr);

    for(; index <= global_allocatedPointers; index++)
        global_allocation[index] = global_allocation[index + 1];

    global_allocatedPointers--;

    free(ptr);
}

void free_all(ptrsize functionID)
{
    for(int index =0; index <= global_allocatedPointers; index++)
        if(global_allocation[index].functionID == functionID)
            abstract_free(global_allocation[index].ptr);

}

void Log_Memory()
{
    printf("LOG_MEMORY\n");
    for(int index = 0; index <= global_allocatedPointers; index++)
    {
        printf("Pointer = %p | size = %d | functionID = %d | Line = %d\n",
               global_allocation[index].ptr,
               global_allocation[index].size,
               global_allocation[index].functionID,
               global_allocation[index].line);
    }
}

